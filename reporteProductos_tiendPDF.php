<?php
  require_once "fpdf/fpdf.php";
  require_once "Logica/Producto_tienda.php";

  $pdf = new FPDF("P", "mm", "Letter");
  $pdf -> SetFont("Courier", "B", 36);
  $pdf -> AddPage();
  $pdf -> Cell(196, 20, "PARCIAL 2", 0, 2, "C");
  $pdf -> SetFont("Times", "U", 15);
  $pdf -> Cell(196, 10, "Reporte de las tiendas del producto", 0, 1, "C");
  $pdf -> ln();
  $pdf -> SetFont("Arial", "", 14);
  $pdf -> Write (10, "Se mostraran los productos que tiene la tienda.");
  $pdf -> ln();

  $idTiend= $_GET["id"];
  $prod_tienda = new Producto_tienda("","",$idTiend);
  $producto_t = $prod_tienda -> consultarProductos_Tiend();


  $pdf -> SetFont("Arial","U",20);
    $pdf -> ln();
    $pdf -> cell(10,10,"#",1);
    $pdf -> cell(70,10,"Producto",1);
    $pdf -> cell(50,10,"Cantidad",1);
    $pdf -> ln();

    $i = 1;
    foreach ($producto_t as $prods) {
      $pdf -> cell(10,40,$i,1);
      $pdf -> cell(70,40,$prods -> getIdProducto_fk(),1);
      $pdf -> cell(50,40,$prods -> getCantidad(),1);
      $pdf -> ln();
      $i++;
    }

  $pdf -> Output();

?>
