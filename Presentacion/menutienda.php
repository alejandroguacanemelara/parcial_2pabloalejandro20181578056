<nav class="navbar navbar-expand-md navbar-light bg-warning">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/inicio.php"); ?>"><i class="fas fa-store"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Producto
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/registrarproducto.php"); ?>">Ingresar producto</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Producto/consultarproducto.php"); ?>">Consultar producto</a>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Tienda
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/registrartienda.php"); ?>">Ingresar tienda</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/consultartienda.php"); ?>">Consultar tiendas</a>
            </div>
          </li>
      </ul>
  </div>
</nav>
