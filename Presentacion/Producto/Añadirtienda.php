<?php
  $idproducto= $_GET["id"];
  $tienda = new Tienda();
  $arraytiendas = $tienda -> consultarTiendas();

  $tiendaselect  = "";
  if(isset($_POST["combo_tienda"])){
    $tiendaselect  = $_POST["combo_tienda"];
  }

  $cantidad  = "";
  if(isset($_POST["cantidad"])){
    $cantidad = $_POST["cantidad"];
  }

  if(isset($_POST["crear"])){
    $prod_tienda = new Producto_tienda("",$idproducto,$tiendaselect,$cantidad);
    $prod_tienda -> insertar();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header text-center bg-warning">
                      <h2 style="font-family: 'Fondamento', cursive; font-size:30px">AÑADIR TIENDA A PRODUCTO</h2>
                  </div>
                  <div class="card-body">
                      <div class="row">
                            <div class="col-12">
                                <form class="" action="index.php?pid=<?php echo base64_encode("Presentacion/Producto/Añadirtienda.php")?><?php echo "&id=".$idproducto ?>" method="post">
                                    <div class="form-group">
                                    <strong><label>TIENDAS</label></strong>
                                      <select class="form-control" name="combo_tienda">
                                        <?php $i=1;
                                          foreach ($arraytiendas as $lTienda) {
                                            echo "<option value=".$lTienda -> getIdTienda().">".$lTienda -> getNombre()."</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="form group">
                                        <strong><label>CANTIDAD</label></strong>
                                        <input type="number" name="cantidad" class="form-control" placeholder="Digite la cantidad" required>
                                    </div>
                                    <div class="dropdown-divider"></div>
                                    <div class="form group">
                                        <button type="submit" name="crear" class="form-control btn btn-warning">INGRESAR CANTIDAD</button>
                                    </div>
                                    <?php if(isset($_POST["crear"])){ ?>
                                    <div class="dropdown-divider"></div>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                      <strong>Cantidad registrada exitosamente!</strong>
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                          </button>
                                    </div>
                                  <?php } ?>
                                </form>
                            </div>
                      </div>
                  </div>
              </div>
        </div>
    </div>

</div>
