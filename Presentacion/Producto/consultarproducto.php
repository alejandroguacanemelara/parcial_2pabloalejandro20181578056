<?php
  $producto = new Producto();
  $arrayProduct = $producto -> consultarProductos();
?>
<div class="container mt-3 d-flex justify-content-center">
  <div class="col-12 col-lg-9">
    <div class="card">
        <div class="card-header bg-secondary text-white">
            <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de productos</h2>
        </div>
        <div class="card-body">
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                    <tr>
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Precio</th>
                      <th>A. Cantidad</th>
                      <th>Tiendas</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                      foreach ($arrayProduct as $lProduct) {
                          echo "<tr>";
                            echo "<td>".$i."</td>";
                            echo "<td>".$lProduct -> getNombre()."</td>";
                            echo "<td>$ ".$lProduct -> getPrecio()."</td>";
                            echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Producto/Añadirtienda.php") . "&id=" . $lProduct -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='Agregar cantidad'><span class='fas fa-edit'></span></a></td>";
                            ?>
                            <td><a class="dropdown-item" href="reporteTiendas_producPDF.php?id=<?php echo $lProduct -> getIdProducto() ?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
                          <?php
                            echo "</tr>";
                          $i++;
                      }
                    ?>
                </tbody>
          </table>
        </div>
    </div>
  </div>
</div>
