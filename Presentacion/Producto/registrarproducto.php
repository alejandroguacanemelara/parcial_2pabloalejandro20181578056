<?php
  $nombre = "";
  if(isset($_POST["nombre"])){
        $nombre = $_POST["nombre"];
  }

  $precio = "";
  if(isset($_POST["precio"])){
        $precio = $_POST["precio"];
  }

  if(isset($_POST["crear"])){

    $producto = new Producto("",$nombre, $precio);
    $producto -> insertar();
  }
?>
<div class="container mt-3 ">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-warning text-white">
                      <h2>REGISTRAR NUEVO PRODUCTO</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("Presentacion/Producto/registrarproducto.php")?>" method="post">
                      <div class="row d-flex justify-content-center">
                          <div class="col-12 col-md-7">
                            <div class="form group">
                                <strong><label>NOMBRE</label></strong>
                                <input type="text" name="nombre" class="form-control" required>
                            </div>
                            <div class="form group">
                                <strong><label>PRECIO</label></strong>
                                <input type="number" name="precio" class="form-control" value="<?php echo $precio ?>" required>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="form group">
                                <button type="submit" name="crear" class="form-control btn btn-outline-warning">REGISTRAR PRODUCTO</button>
                            </div>
                            <?php if(isset($_POST["crear"])){ ?>
                            <div class="dropdown-divider"></div>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              <strong>Producto creado exitosamente!</strong>
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                  </button>
                            </div>
                            <?php } ?>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
    </div>
</div>
