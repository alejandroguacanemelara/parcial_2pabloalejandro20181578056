<?php
  $nombre = "";
  if(isset($_POST["nombre"])){
        $nombre = $_POST["nombre"];
  }

  $direccion = "";
  if(isset($_POST["direccion"])){
        $direccion = $_POST["direccion"];
  }

  if(isset($_POST["crear"])){

    $tienda = new Tienda("",$nombre, $direccion);
    $tienda -> insertar();
  }
?>
<div class="container mt-3 ">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-warning text-white">
                      <h2>REGISTRAR NUEVA TIENDA</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("Presentacion/Tienda/registrartienda.php")?>" method="post">
                      <div class="row d-flex justify-content-center">
                          <div class="col-12 col-md-7">
                            <div class="form group">
                                <strong><label>NOMBRE</label></strong>
                                <input type="text" name="nombre" class="form-control" required>
                            </div>
                            <div class="form group">
                                <strong><label>DIRECCION</label></strong>
                                <input type="text" name="direccion" class="form-control"  required>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="form group">
                                <button type="submit" name="crear" class="form-control btn btn-outline-warning">REGISTRAR TIENDA</button>
                            </div>
                            <?php if(isset($_POST["crear"])){ ?>
                            <div class="dropdown-divider"></div>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              <strong>Tienda creada exitosamente!</strong>
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                  </button>
                            </div>
                            <?php } ?>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
    </div>
</div>
