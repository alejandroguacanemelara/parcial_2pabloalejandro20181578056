<?php
  $tienda = new Tienda();
  $arrayTienda = $tienda -> consultarTiendas();
?>
<div class="container mt-3 d-flex justify-content-center">
  <div class="col-12 col-lg-9">
    <div class="card">
        <div class="card-header bg-secondary text-white">
            <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de tiendas</h2>
        </div>
        <div class="card-body">
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                    <tr>
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Direccion</th>
                      <th>--</th>
                      <th>Productos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                      foreach ($arrayTienda as $lTienda) {
                          echo "<tr>";
                            echo "<td>".$i."</td>";
                            echo "<td>".$lTienda -> getNombre()."</td>";
                            echo "<td>$ ".$lTienda -> getDireccion()."</td>";
                            echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Tienda/Añadirproducto.php") . "&id=" . $lTienda -> getIdTienda(). "' data-toggle='tooltip' data-placement='left' title='Agregar cantidad'><span class='fas fa-edit'></span></a></td>";
                            ?>
                            <td><a href="reporteProductos_tiendPDF.php?id=<?php echo $lTienda -> getIdTienda() ?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
                          <?php
                            echo "</tr>";
                          $i++;
                      }
                    ?>
                </tbody>
          </table>
        </div>
    </div>
  </div>
</div>
