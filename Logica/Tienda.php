<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/TiendaDAO.php";

  class Tienda{

    private $idTienda;
    private $nombre;
    private $direccion;
    private $conexion;
    private $tiendaDAO;

        public function getIdTienda(){
            return $this -> idTienda;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getDireccion(){
            return $this -> direccion;
        }

        public function Tienda($idTienda="",$nombre="",$direccion=""){
              $this -> idTienda = $idTienda;
              $this -> nombre = $nombre;
              $this -> direccion = $direccion;
              $this -> conexion = new Conexion();
              $this -> tiendaDAO = new TiendaDAO($this -> idTienda,$this -> nombre, $this -> direccion);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> tiendaDAO -> insertar());
          $this -> conexion -> cerrar();
        }


        public function consultarTiendas(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarTiendas());
          $tienarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $tien = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($tienarray,$tien);
          }
          $this -> conexion -> cerrar();
          return $tienarray;
        }


  }

?>
