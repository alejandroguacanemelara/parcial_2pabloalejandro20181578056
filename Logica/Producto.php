<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ProductoDAO.php";

  class Producto{

    private $idProducto;
    private $nombre;
    private $precio;
    private $conexion;
    private $productoDAO;

        public function getIdProducto(){
            return $this -> idProducto;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getPrecio(){
            return $this -> precio;
        }

        public function Producto($idProducto="",$nombre="",$precio=""){
              $this -> idProducto = $idProducto;
              $this -> nombre = $nombre;
              $this -> precio = $precio;
              $this -> conexion = new Conexion();
              $this -> productoDAO = new ProductoDAO($this -> idProducto,$this -> nombre, $this -> precio);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> insertar());
          //$this -> productoDAO -> insertar();
          $this -> conexion -> cerrar();
        }


        public function consultarProductos(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductos());
          $proarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $pro = new Producto($resultado[0], $resultado[1], $resultado[2]);
            array_push($proarray,$pro);
          }
          $this -> conexion -> cerrar();
          return $proarray;
        }


  }

?>
