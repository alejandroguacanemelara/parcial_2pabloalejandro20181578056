<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/Producto_tiendaDAO.php";

  class Producto_tienda{

    private $id;
    private $idProducto_fk;
    private $idTienda_fk;
    private $cantidad;
    private $conexion;
    private $producto_tiendaDAO;

        public function getId(){
            return $this -> id;
        }

        public function getIdProducto_fk(){
            return $this -> idProducto_fk;
        }

        public function getIdTienda_fk(){
            return $this -> idTienda_fk;
        }

        public function getCantidad(){
            return $this -> cantidad;
        }

        public function Producto_tienda($id="",$idProducto_fk="",$idTienda_fk="",$cantidad=""){
              $this -> id = $id;
              $this -> idProducto_fk = $idProducto_fk;
              $this -> idTienda_fk = $idTienda_fk;
              $this -> cantidad = $cantidad;
              $this -> conexion = new Conexion();
              $this -> producto_tiendaDAO = new Producto_tiendaDAO($this -> id,$this -> idProducto_fk,$this -> idTienda_fk,$this -> cantidad);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> producto_tiendaDAO -> insertar());
          $this -> conexion -> cerrar();
        }

        public function consultar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> producto_tiendaDAO -> consultar());
          $protienarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prod_tienda = new Producto_tienda("",$resultado[0], $resultado[1], $resultado[2]);
            array_push($protienarray,$prod_tienda);
          }
          $this -> conexion -> cerrar();
          return $protienarray;
        }

        public function consultarTiendas_Prod(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> producto_tiendaDAO -> consultarTiendas_Prod());
          $protienarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prod_tienda = new Producto_tienda("","", $resultado[0], $resultado[1]);
            array_push($protienarray,$prod_tienda);
          }
          $this -> conexion -> cerrar();
          return $protienarray;
        }

        public function consultarProductos_Tiend(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> producto_tiendaDAO -> consultarProductos_Tiend());
          $protienarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prod_tienda = new Producto_tienda("", $resultado[0],"", $resultado[1]);
            array_push($protienarray,$prod_tienda);
          }
          $this -> conexion -> cerrar();
          return $protienarray;
        }

  }

?>
